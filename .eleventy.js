const toml = require("toml");

module.exports = eleventyConfig => {
  eleventyConfig.on('eleventy.before', async ({ dir, runMode, outputMode }) => {
    // Run me before the build starts
  });
  
  eleventyConfig.addDataExtension("toml", contents => toml.parse(contents));
  eleventyConfig.setTemplateFormats("11ty.js");
  eleventyConfig.addPassthroughCopy("template/images");
  eleventyConfig.addPassthroughCopy("template/css");
  eleventyConfig.addPassthroughCopy("template/js");
  eleventyConfig.addPassthroughCopy("template/favicon.ico");
  eleventyConfig.addPassthroughCopy("template/robots.txt");
  eleventyConfig.addPassthroughCopy({ "src/images": "images" });
  eleventyConfig.addPassthroughCopy({ "src/*.html": "/" });
  eleventyConfig.addPassthroughCopy("static");
  eleventyConfig.addPassthroughCopy("content");
  
  return {
    dir: {
      input: "template",
      data: "../src",
      output: "public"
    }     
  }  
};
