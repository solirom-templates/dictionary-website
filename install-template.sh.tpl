
template_filename="template.zip"
template_temp_dir="./template-tmp"

wget https://gitlab.com/solirom-templates/dictionary-website/-/archive/master/dictionary-website-master.zip -O "$template_filename"
unzip "$template_filename" -d $template_temp_dir
cp -r $template_temp_dir/*/. .
rm -rf $template_temp_dir
mv .gitlab-ci-template.yml .gitlab-ci.yml
rm "$template_filename"