solirom = {};
solirom.data = {};

solirom.data.editions = document.querySelector("solirom-editions").editions;

const editionsNumbers = Object.keys(solirom.data.editions);

if (editionsNumbers.length === 1) {
    solirom.data.editionBaseURL = solirom.data.editions[editionsNumbers[0]];
}
window.loadingSpinner.show();
let indexes_definitions = await fetch(`${solirom.data.editionBaseURL}indexes/text/indexes-metadata.json`).then(response => response.json());

// get the data for the heawords index
let headwords_fst = await fetch(`${solirom.data.editionBaseURL}indexes/text/headwords/index.fst`).then(response => response.arrayBuffer());
headwords_fst = new Uint8Array(headwords_fst);
let headwords_inverted_index = await fetch(`${solirom.data.editionBaseURL}indexes/text/headwords/index.json`).then(response => response.json());

// get the cross references
let cross_references = await fetch(`${solirom.data.editionBaseURL}indexes/text/cross-references.json`).then(response => response.json());

//document.dispatchEvent(new CustomEvent("solirom:vertical-menu:selected", {"detail": "home.html"}));

document.dispatchEvent(new CustomEvent("solirom:fst-index-search:set", {
    "detail": {
        "indexBaseURL": `${solirom.data.editionBaseURL}indexes/text/`,
        "fst": headwords_fst,
        "inverted_index": headwords_inverted_index,
        "cross_references": cross_references
    }
}));
document.dispatchEvent(new CustomEvent("solirom:json-index-search:index", {
    "detail": {
        "indexBaseURL": `${solirom.data.editionBaseURL}indexes/text/`,
        "cross_references": cross_references,
        "indexes_definitions": indexes_definitions,
    },
}));
window.loadingSpinner.hide();

document.addEventListener("solirom:vertical-menu:selected", async event => {
    const url = event.detail;
    if (url === "") {
        return;
    }
    
    if (url === "#home") {
        location.reload();
    }    
    
    const mainContentContainer = document.getElementById("main-content");
    const secondaryContentContainer = document.getElementById("secondary-content");
    if (url === "#search") {
        mainContentContainer.style.display = "grid";
        secondaryContentContainer.style.display = "none";

        return;
    }

    const secondaryContent = await fetch(url).then((response) => response.text());
    secondaryContentContainer.innerHTML = "";
    secondaryContentContainer.insertAdjacentHTML("afterbegin", secondaryContent);

    mainContentContainer.style.display = "none";
    secondaryContentContainer.style.display = "inline";
});

document.addEventListener("click", event => {
    const target = event.target;

    if (target.matches("button#show-search-result")) {
        document.dispatchEvent(new CustomEvent("solirom:search-result:show"));
        document.dispatchEvent(new CustomEvent("solirom:dictionary-entry:hide"));
        document.getElementById("main-content-toolbar").style.visibility = "hidden";
    }
});

document.addEventListener("solirom:fst-index-search:selected", event => {
    document.dispatchEvent(new CustomEvent("solirom:search-result:hide"));
    document.dispatchEvent(new CustomEvent("solirom:dictionary-entry:selected", {
        "detail": event.detail
    }));
});

document.addEventListener("solirom:search-result:selected", (event) => {
    document.getElementById("main-content-toolbar").style.visibility = "visible";
    document.dispatchEvent(new CustomEvent("solirom:search-result:hide"));
    document.dispatchEvent(new CustomEvent("solirom:dictionary-entry:selected", {
        "detail": event.detail
    }));
});

document.addEventListener("solirom:fst-index-search:result", event => {
    document.dispatchEvent(new CustomEvent("solirom:dictionary-entry:hide"));
    document.dispatchEvent(new CustomEvent("solirom:search-result:new", {
        "detail": event.detail
    }));
});

document.addEventListener("solirom:json-index-search:result", event => {
    document.dispatchEvent(new CustomEvent("solirom:dictionary-entry:hide"));
    document.dispatchEvent(new CustomEvent("solirom:json-index-search-result:new", {
        "detail": event.detail
    }));
});

document.addEventListener("solirom:vertical-menu:selected", async event => {
    const url = event.detail;
    if (url === "") {
        return;
    }

    const mainContentContainer = document.getElementById("main-content");
    const secondaryContentContainer = document.getElementById("secondary-content");
    if (url === "#search") {
        mainContentContainer.style.display = "grid";
        secondaryContentContainer.style.display = "none";

        return;
    }

    const secondaryContent = await fetch(url).then((response) => response.text());
    secondaryContentContainer.innerHTML = "";
    secondaryContentContainer.insertAdjacentHTML("afterbegin", secondaryContent);

    mainContentContainer.style.display = "none";
    secondaryContentContainer.style.display = "inline";
});
