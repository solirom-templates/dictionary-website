const fs = require("fs");
const path = require('path');

//const homeArticle = fs.readFileSync(path.resolve(".", "src/home.html"), "utf8");

const isDevelopment = process.env.NODE_ENV === `development`;

exports.render = (data) => {
	data = data.index;

	if (isDevelopment) {
		data.editions[1]["base-url"] = "http://localhost:8080/";
	}
	let baseURL = data.editions[1]["base-url"];

	let verticalMenuTemplate = generateVerticalMenu(data.verticalMenu);

	var indexPage =
		`
	<!doctype html>
	<html lang="ro">
		<head> 
			<title>${data.header.title}</title>
			<meta charset="utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1"/>	
			<meta name="description" content="${data.header.title}"/>
			<meta name="author" content="Claudius Teodorescu"/>	
			<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
			<link href="https://fonts.cdnfonts.com/css/palatino-linotype" rel="stylesheet" />
			<link rel="stylesheet" href="css/index.css" />
			<script src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.5.6/dialog-polyfill.min.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/loading-spinner/index.min.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/footer/index.min.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/search-result/index.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/header/index.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/editions/index.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/dictionary-entry/index.js"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/fst-index-search/index.mjs"></script>
			<script type="module" src="https://solirom.gitlab.io/web-components/json-index-search/index.mjs"></script>
			<script type="module" src="https://cdn.jsdelivr.net/npm/solirom-cdn@2.6.1/vertical-menu/index.js"></script>
			<script type="module" src="js/index.js"></script>
		</head>
		<body>
			<solirom-header>
				<template>
					<div id="container">
						<div id="logo"><a href="https://acad.ro/" target="_blank" title="Academia Română"><img src="https://acad.ro/academia_romana/img/ACAD_260.png" alt="Academia Română"></a></div>
						<div id="title"><span>${data.header.title}</span><span></span></div>
						<div id="varia"></div>
					</div> 
				</template>			
			</solirom-header>
			<main id="content-container">
				<solirom-vertical-menu>${verticalMenuTemplate}</solirom-vertical-menu>
				<div id="main-content">
					<solirom-editions><template><div id="container"><button class="edition" data-number="1" data-url="${baseURL}" title="Ediția 1">1</button></div></template></solirom-editions>
					<sc-fst-index-search show-first-letters="false" index-relative-path="headwords"></sc-fst-index-search>
					<div id="main-content-toolbar"><button id="show-search-result">←</button></div>
					<sc-dictionary-entry></sc-dictionary-entry>
					<sc-search-result></sc-search-result>
					<sc-json-index-search></sc-json-index-search>
				</div>
				<div id="secondary-content"></div>
			</main>
			<solirom-footer></solirom-footer>
			<solirom-loading-spinner></solirom-loading-spinner>
		</body>
	</html>
	`
		;

	return indexPage;
};

/*
* all the functions below are to be removed
* when the template will be upgraded to be multilingual
*/

const subMenuTemplate = data =>
`
<li class="sub_menu--item">
	<span class="sub_menu--label" data-url="${data.url}">${data.label}</span>
 </li>
`
;
const componentTemplate = data =>
`
<template>
    <nav>
        <ul class="menu">${data}</ul>
    </nav>
</template>

`
;

const generateSubMenu = menuItemObject => {
    const subMenuObject = menuItemObject.submenu;
    
    var subMenu = "";
    var subMenuExists = false;
    if (subMenuObject) {
        subMenu = Object.keys(subMenuObject).map(subMenuItem => subMenuTemplate(subMenuObject[subMenuItem])).join("");
        subMenu = `<ul class="sub_menu">${subMenu}</ul>`;
        subMenuExists = true;
    }
    
    return {"exists": subMenuExists, "contents": subMenu};
};

const generateMenuItem = menuItemObject => {
    const menuLabel = menuItemObject.label;
    const menuUrl = menuItemObject.url || "";  
    const menuTitle = menuItemObject.title || menuLabel;
    const subMenu = generateSubMenu(menuItemObject);
    
    var menuItemContent = "";    
    if (subMenu.exists) {
        menuItemContent =
        `<li class="menu--item menu--item__has_sub_menu">
            <label class="menu--link" title="${menuTitle}">
                <span class="menu--label">${menuLabel} <span class="collapse-submenu-arrow">▼</span></span>
            </label>
            ${subMenu.contents}
        </li>`;         
    } else {
        menuItemContent = 
        `
        <li class="menu--item">
            <label class="menu--link" title="${menuTitle}">
                <span class="menu--label" data-url="${menuUrl}">${menuLabel}</span>
            </label>
        </li>`;           
    }  
    
    return menuItemContent;
};

const generateVerticalMenu = (menuObject) => {
	return componentTemplate(Object.entries(menuObject).map(menuItemObject => generateMenuItem(menuItemObject[1])).join(""));
};
